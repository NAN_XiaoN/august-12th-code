#include<stdio.h>
//int main()
//{
//	char murder = 0;
//	for (murder = 'A'; murder <= 'D'; murder++)
//	{
//		if ((murder != 'A') +
//			(murder == 'C') +
//			(murder == 'D') +
//			(murder != 'D') == 3)
//			printf("%c是杀人犯\n", murder);
//	}
//	return 0;
//}
//杨辉三角
//int main()
//{
//	int arr[10][10];
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 10; i++)
//	{
//		for (j = 0; j < 10; j++)
//		{
//			arr[i][j] = 1;
//		}
//	}
//	for (i = 1; i < 10; i++)
//	{
//		for (j = 1; j < i; j++)
//		{
//			arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//		}
//	}
//	for (i = 0; i < 9; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			printf("%6d", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
//杨氏矩阵
int FindNum2(int arr[][3], int* px, int* py, int k)
{
	int x = 0;
	int y = *py - 1;
	while (x < *px && y >= 0)
	{
		if (arr[x][y] < k)
		{
			x++;
		}
		else if (arr[x][y] > k)
		{
			y--;
		}
		else
		{
			//这里找到了k，此时再将k的坐标赋值带回去
			*px = x;
			*py = y;
			return 1;
		}
	}
	return 0;
}
int main()
{
	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
	int k = 7;
	int x = 3;//行
	int y = 3;//列
	//这里的&x，&y的作用是
	//1.传入参数
	//2.带回值
	//这种参数的设计叫做返回型参数	
	int ret = FindNum2(arr, &x, &y, k);//改进处
	if (1 == ret)
	{
		printf("找到了\n");
		printf("%d, %d\n", x, y);
	}
	else
	{
		printf("找不到\n");
	}
	return 0;
}


